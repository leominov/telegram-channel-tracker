package main

import (
	"encoding/json"
	"gopkg.in/yaml.v3"
	"os"
	"time"
)

type Config struct {
	Interval        time.Duration `yaml:"interval"`
	Channels        []Channel     `yaml:"channels"`
	OutputDirectory string        `yaml:"outputDirectory"`
}

type Channel struct {
	Name string `yaml:"name"`
	URL  string `yaml:"url"`
}

func LoadFromFile(filename string) (*Config, error) {
	b, err := os.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	var c *Config
	err = yaml.Unmarshal(b, &c)
	if err != nil {
		return nil, err
	}
	return c, nil
}

func (c *Config) String() string {
	b, _ := json.Marshal(c)
	return string(b)
}
