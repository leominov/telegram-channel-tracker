package main

import (
	"io"
	"net/http"
	"regexp"
	"strconv"
	"strings"
)

var (
	subscribersRE = regexp.MustCompile(`<div class="tgme_page_extra">(.*) subscribers</div>`)
)

func GetChannelSubscriptions(url string) (int64, error) {
	resp, err := http.Get(url)
	if err != nil {
		return 0, err
	}
	b, err := io.ReadAll(resp.Body)
	if err != nil {
		return 0, err
	}
	defer resp.Body.Close()
	matches := subscribersRE.FindStringSubmatch(string(b))
	if len(matches) < 2 {
		return 0, nil
	}
	countRaw := strings.Replace(matches[1], " ", "", -1)
	count, err := strconv.ParseInt(countRaw, 10, 64)
	if err != nil {
		return 0, err
	}
	return count, nil
}
