package main

import (
	"flag"
	"log"
	"os"
	"os/signal"
	"syscall"
)

var (
	configFile = flag.String("c", "config.yaml", "Path to configuration file")
)

func main() {
	flag.Parse()

	config, err := LoadFromFile(*configFile)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Configuration: %s", config.String())

	worker := NewWorker(config)
	worker.Start()

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt, syscall.SIGTERM)
	sig := <-stop

	log.Printf("Received termination signal %q", sig.String())
	worker.Stop()
	log.Println("Bye")
}
