package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"sync/atomic"
	"time"
)

type Worker struct {
	c         *Config
	ticker    *time.Ticker
	isRunning int32
	stopCh    chan struct{}
	stopped   chan struct{}
}

func NewWorker(config *Config) *Worker {
	return &Worker{
		c:       config,
		ticker:  time.NewTicker(config.Interval),
		stopCh:  make(chan struct{}),
		stopped: make(chan struct{}),
	}
}

func (w *Worker) processChannel(ch Channel) {
	var addHeader bool

	year, week := time.Now().ISOWeek()
	filename := filepath.Join(w.c.OutputDirectory, fmt.Sprintf("%s-%d-%d.csv", ch.Name, year, week))
	if _, err := os.Stat(filename); err != nil {
		addHeader = true
	}

	f, err := os.OpenFile(filename, os.O_WRONLY|os.O_CREATE|os.O_APPEND, os.ModePerm)
	if err != nil {
		log.Println(err)
		return
	}
	defer f.Close()

	total, err := GetChannelSubscriptions(ch.URL)
	if err != nil {
		log.Println(err)
		return
	}

	if addHeader {
		_, err = f.WriteString("Time,Subscriptions\n")
		if err != nil {
			log.Println(err)
			return
		}
	}

	_, err = f.WriteString(fmt.Sprintf("%s,%d\n", time.Now().UTC().Format(time.RFC3339), total))
	if err != nil {
		log.Println(err)
	}
}

func (w *Worker) processList() {
	for _, ch := range w.c.Channels {
		w.processChannel(ch)
	}
}

func (w *Worker) start() {
	w.processList()
	for {
		select {
		case <-w.ticker.C:
			w.processList()
		case <-w.stopCh:
			w.ticker.Stop()
			w.stopped <- struct{}{}
			return
		}
	}
}

func (w *Worker) Start() {
	if atomic.CompareAndSwapInt32(&w.isRunning, 0, 1) {
		go w.start()
	}
}

func (w *Worker) Stop() {
	if atomic.CompareAndSwapInt32(&w.isRunning, 1, 0) {
		w.stopCh <- struct{}{}
		<-w.stopped
	}
}
