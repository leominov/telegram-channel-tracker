.PHONY: pi
pi:
	@GOOS=linux GOARCH=arm GOARM=5 go build ./
	ssh root@raspberrypi.local mkdir -p /opt/telegram-channel-tracker/
	ssh root@raspberrypi.local systemctl stop telegram-channel-tracker || true
	scp ./telegram-channel-tracker root@raspberrypi.local:/opt/telegram-channel-tracker/
	scp ./telegram-channel-tracker.config.yaml root@raspberrypi.local:/opt/telegram-channel-tracker/config.yaml
	scp ./telegram-channel-tracker.service root@raspberrypi.local:/etc/systemd/system/
	ssh root@raspberrypi.local systemctl enable telegram-channel-tracker
	ssh root@raspberrypi.local systemctl start telegram-channel-tracker
